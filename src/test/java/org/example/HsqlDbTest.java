package org.example;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/** Test of using HSQL database.
 * 
 * @author Maxim Rodyushkin */
public class HsqlDbTest {
  /** Connection to database. */
  private transient Connection conn;

  /** Default constructor. */
  protected HsqlDbTest() {
    super();
  }

  /** Test in-memory database connection. */
  @Test
  public final void inMemorytest() {
    assertThat("DB Connection", this.conn, is(notNullValue()));
  }

  /** Set up test database.
   * 
   * @throws SQLException
   *           but test handles it. */
  @Before
  public final void setUp() throws SQLException {
    this.conn = DriverManager
        .getConnection("jdbc:hsqldb:mem:inMemorytest;shutdown=true", "", "");
  }

  /** Remove database.
   * 
   * @throws SQLException
   *           but test handles it */
  @After
  public final void tearDown() throws SQLException {
    if (this.conn != null && !this.conn.isClosed()) {
      this.conn.close();
    }
  }
}
